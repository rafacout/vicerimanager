﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using Viceri.Project.MVC.ViewModels;

namespace Viceri.Project.MVC.Util
{
    public static class HttpUtil
    {
        public static List<T> GetList<T>(string url)
        {
            List<T> list = new List<T>();
            HttpClient client = new HttpClient();

            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            try
            {
                HttpResponseMessage responseGet = client.GetAsync(url).Result;

                if (responseGet.IsSuccessStatusCode)
                {
                    var dataObjects = responseGet.Content.ReadAsAsync<IEnumerable<T>>().Result;

                    foreach (var p in dataObjects)
                    {
                        list.Add(p);
                    }
                }
                else
                {
                    var message = responseGet.Content.ReadAsStringAsync();
                    var error = JsonConvert.DeserializeObject<ErrorMessageViewModel>(message.Result);
                    throw new ApplicationException(error.Message);
                }

                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                client.Dispose();
            }
        }

        public static T Get<T>(string url)
        {
            T dataObject;
            HttpClient client = new HttpClient();

            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            try
            {
                HttpResponseMessage responseGet = client.GetAsync(url).Result;

                if (responseGet.IsSuccessStatusCode)
                {
                    dataObject = responseGet.Content.ReadAsAsync<T>().Result;

                    return dataObject;
                }
                else
                {
                    var message = responseGet.Content.ReadAsStringAsync();
                    var error = JsonConvert.DeserializeObject<ErrorMessageViewModel>(message.Result);
                    throw new ApplicationException(error.Message);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                client.Dispose();
            }
        }

        public static T Get<T>(string url, int id)
        {
            List<T> list = new List<T>();
            HttpClient client = new HttpClient();

            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            try
            {
                HttpResponseMessage responseGet = client.GetAsync(url + id).Result;

                if (responseGet.IsSuccessStatusCode)
                {
                    var dataObject = responseGet.Content.ReadAsAsync<T>().Result;

                    return dataObject;
                }
                else
                {
                    var message = responseGet.Content.ReadAsStringAsync();
                    var error = JsonConvert.DeserializeObject<ErrorMessageViewModel>(message.Result);
                    throw new ApplicationException(error.Message);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                client.Dispose();
            }
        }

        public static bool Post<T>(string url, T item)
        {
            HttpClient client = new HttpClient();

            try
            {
                string jsonObject = JsonConvert.SerializeObject(item);

                HttpResponseMessage responsePost = client.PostAsync(url, new StringContent(jsonObject, Encoding.UTF8, "application/json")).Result;

                if (responsePost.IsSuccessStatusCode)
                {
                    return true;
                }
                else
                {
                    var message = responsePost.Content.ReadAsStringAsync();
                    var error = JsonConvert.DeserializeObject<ErrorMessageViewModel>(message.Result);
                    throw new ApplicationException(error.Message);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                client.Dispose();
            }
        }

        public static bool Put<T>(string url, T item)
        {
            HttpClient client = new HttpClient();

            try
            {
                string jsonObject = JsonConvert.SerializeObject(item);

                HttpResponseMessage responsePut = client.PutAsync(url, new StringContent(jsonObject, Encoding.UTF8, "application/json")).Result;

                if (responsePut.IsSuccessStatusCode)
                {
                    return true;
                }
                else
                {
                    var message = responsePut.Content.ReadAsStringAsync();
                    var error = JsonConvert.DeserializeObject<ErrorMessageViewModel>(message.Result);
                    throw new ApplicationException(error.Message);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                client.Dispose();
            }
        }

        public static bool Delete(string url, int id)
        {
            HttpClient client = new HttpClient();

            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            try
            {
                HttpResponseMessage responseDelete = client.DeleteAsync(url + id).Result;

                if (responseDelete.IsSuccessStatusCode)
                {
                    return true;
                }
                else
                {
                    var message = responseDelete.Content.ReadAsStringAsync();
                    var error = JsonConvert.DeserializeObject<ErrorMessageViewModel>(message.Result);
                    throw new ApplicationException(error.Message);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                client.Dispose();
            }
        }
    }
}