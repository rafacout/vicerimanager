﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Viceri.Project.MVC.ViewModels
{
    public class ProjectViewModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "{0} is Required")]
        [Range(1, int.MaxValue, ErrorMessage = "Please enter valid integer Number")]
        [Display(Name = "Project Id")]
        public int ProjectGitId { get; set; }

        [Required(ErrorMessage = "{0} is Required")]
        [StringLength(1000, MinimumLength = 3, ErrorMessage = "{0} size should be between {2} and {1}")]
        [Display(Name = "Project Name")]
        public string Name { get; set; }

        [StringLength(1000, MinimumLength = 3, ErrorMessage = "{0} size should be between {2} and {1}")]
        public string Description { get; set; }

        [Required(ErrorMessage = "{0} is Required")]
        [StringLength(1000, MinimumLength = 3, ErrorMessage = "{0} size should be between {2} and {1}")]
        [Display(Name = "URL")]
        [Url(ErrorMessage = "{0} is invalid")]
        public string Web_URL { get; set; }

        [Display(Name = "Imported?")]
        public bool IsImported { get; set; }

        [Display(Name = "Open Issues Count")]
        public int Open_Issues_Count { get; set; }
    }
}