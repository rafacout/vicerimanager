﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Viceri.Project.MVC.ViewModels
{
    public class IssueViewModel
    {
        public IssueViewModel()
        {
            this.ProjectsList = new List<SelectListItem>();
        }
        public int Id { get; set; }

        [Required(ErrorMessage = "{0} is Required")]
        [Range(1, int.MaxValue, ErrorMessage = "Please enter valid integer Number")]
        [Display(Name = "Issue Id")]
        public int IssueGitId { get; set; }

        [Required(ErrorMessage = "{0} is Required")]
        [StringLength(1000, MinimumLength = 3, ErrorMessage = "{0} size should be between {2} and {1}")]
        [Display(Name = "Issue Title")]
        public string Title { get; set; }

        [Required(ErrorMessage = "{0} is Required")]
        [Display(Name ="Created Date")]
        public DateTime Created_At { get; set; }

        [Display(Name = "Closed Date")]
        public DateTime Closed_At { get; set; }

        [Required(ErrorMessage = "{0} is Required")]
        public string State { get; set; }

        [Required(ErrorMessage = "{0} is Required")]
        [Display(Name = "Project")]
        public int ProjectId { get; set; }

        public ProjectViewModel Project { get; set; }

        public List<SelectListItem> ProjectsList { get; set; }

    }
}