﻿using System;
using System.Configuration;
using System.Web.Mvc;
using Viceri.Project.MVC.Util;
using Viceri.Project.MVC.ViewModels;

namespace Viceri.Project.MVC.Controllers
{
    public class ProjectsController : Controller
    {
        private string _projects_api_url;
        private string _import_projects_api_url;

        public ProjectsController()
        {
            _projects_api_url = ConfigurationManager.AppSettings["manager_api_url"] + "projects/";
            _import_projects_api_url = ConfigurationManager.AppSettings["manager_api_url"] + "importprojects/";
        }

        public ActionResult Index()
        {
            try
            {
                var projects = HttpUtil.GetList<ProjectViewModel>(_projects_api_url);

                return View(projects);
            }
            catch (Exception ex)
            {
                ViewData["ErrorMessage"] = ex.Message;
                return View();
            }
        }

        public ActionResult Details(int id)
        {
            try
            {
                var project = HttpUtil.Get<ProjectViewModel>(_projects_api_url, id);

                return View(project);

            }
            catch (Exception ex)
            {
                ViewData["ErrorMessage"] = ex.Message;
                return View();
            }
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ProjectViewModel project)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    HttpUtil.Post<ProjectViewModel>(_projects_api_url, project);
                }

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ViewData["ErrorMessage"] = ex.Message;
                return View(project);
            }
        }

        public ActionResult Edit(int id)
        {
            try
            {
                var project = HttpUtil.Get<ProjectViewModel>(_projects_api_url, id);

                return View(project);
            }
            catch (Exception ex)
            {
                ViewData["ErrorMessage"] = ex.Message;
                return View();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ProjectViewModel project)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var returnHttp = HttpUtil.Put<ProjectViewModel>(_projects_api_url, project);
                }

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ViewData["ErrorMessage"] = ex.Message;
                return View(project);
            }
        }

        public ActionResult Delete(int id)
        {
            try
            {
                var project = HttpUtil.Get<ProjectViewModel>(_projects_api_url, id);

                return View(project);
            }
            catch (Exception ex)
            {
                ViewData["ErrorMessage"] = ex.Message;
                return View();
            }
        }

        [HttpPost]
        public ActionResult Delete(ProjectViewModel project)
        {
            try
            {
                HttpUtil.Delete(_projects_api_url, project.Id);

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ViewData["ErrorMessage"] = ex.Message;
                return View();
            }
        }

        [HttpGet]
        public ActionResult ImportProjects()
        {
            try
            {
                var isSuccess = HttpUtil.Get<Boolean>(_import_projects_api_url);

                return Json("Success", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("Error: " + ex.Message, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
