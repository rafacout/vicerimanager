﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;
using Viceri.Project.MVC.Util;
using Viceri.Project.MVC.ViewModels;

namespace Viceri.Project.MVC.Controllers
{
    public class IssuesController : Controller
    {
        private string _issues_api_url;
        private string _projects_api_url;

        public IssuesController()
        {
            _issues_api_url = ConfigurationManager.AppSettings["manager_api_url"] + "issues/";
            _projects_api_url = ConfigurationManager.AppSettings["manager_api_url"] + "projects/";
        }

        public ActionResult Index()
        {
            try
            {
                var issues = HttpUtil.GetList<IssueViewModel>(_issues_api_url);

                return View(issues);
            }
            catch (Exception ex)
            {
                ViewData["ErrorMessage"] = ex.Message;
                return View();
            }
        }

        public ActionResult Details(int id)
        {
            try
            {
                var issue = HttpUtil.Get<IssueViewModel>(_issues_api_url, id);

                return View(issue);
            }
            catch (Exception ex)
            {
                ViewData["ErrorMessage"] = ex.Message;
                return View();
            }
        }

        public ActionResult Create()
        {
            try
            {
                IssueViewModel issue = new IssueViewModel();

                issue.ProjectsList = listProjects(0);

                return View(issue);
            }
            catch (Exception ex)
            {
                ViewData["ErrorMessage"] = ex.Message;
                return View();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IssueViewModel issue)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    HttpUtil.Post<IssueViewModel>(_issues_api_url, issue);
                }

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ViewData["ErrorMessage"] = ex.Message;
                return View(issue);
            }
        }

        public ActionResult Edit(int id)
        {
            try
            {
                var issue = HttpUtil.Get<IssueViewModel>(_issues_api_url, id);

                issue.ProjectsList = listProjects(issue.ProjectId);

                return View(issue);
            }
            catch (Exception ex)
            {
                ViewData["ErrorMessage"] = ex.Message;
                return View();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(IssueViewModel issue)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var returnHttp = HttpUtil.Put<IssueViewModel>(_issues_api_url, issue);
                }

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ViewData["ErrorMessage"] = ex.Message;
                return View(issue);
            }
        }

        public ActionResult Delete(int id)
        {
            try
            {
                var issue = HttpUtil.Get<IssueViewModel>(_issues_api_url, id);

                return View(issue);
            }
            catch (Exception ex)
            {
                ViewData["ErrorMessage"] = ex.Message;
                return View();
            }
        }

        [HttpPost]
        public ActionResult Delete(IssueViewModel issue)
        {
            try
            {
                HttpUtil.Delete(_issues_api_url, issue.Id);

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ViewData["ErrorMessage"] = ex.Message;
                return View("Index");
            }
        }

        private List<SelectListItem> listProjects(int projectId)
        {
            return (from a in HttpUtil.GetList<ProjectViewModel>(_projects_api_url)
                    orderby a.Name
                    select new SelectListItem()
                    {
                        Text = a.ProjectGitId.ToString() + " - " + a.Name,
                        Value = a.Id.ToString(),
                        Selected = a.Id == projectId
                    }).ToList();
        }

    }
}
