﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using Viceri.Project.Manager.Application;
using Viceri.Project.Manager.Application.Interfaces;
using Viceri.Project.Manager.Application.ViewModels;

namespace Viceri.Project.Manager.Controllers
{
    public class IssuesController : ApiController
    {
        private readonly IIssueApplication _issueApp;

        public IssuesController(IIssueApplication issueApp)
        {
            _issueApp = issueApp;
        }

        public IHttpActionResult Get()
        {
            return Ok(_issueApp.GetAll());
        }

        public IHttpActionResult Get(int id)
        {
            try
            {
                var issue = _issueApp.GetById(id);

                if (issue == null)
                {
                    return BadRequest("Issue Not Found");
                }

                return Ok(issue);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        public IHttpActionResult Post([FromBody]IssueViewModel issue)
        {
            try
            {
                if (issue == null)
                {
                    return BadRequest("Issue Not Found");
                }

                _issueApp.AddOrUpdate(issue);

                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        public IHttpActionResult Put([FromBody]IssueViewModel issue)
        {
            try
            {
                if (issue == null)
                {
                    return BadRequest("Issue Not Found");
                }

                _issueApp.AddOrUpdate(issue);

                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        public IHttpActionResult Delete(int id)
        {
            try
            {
                _issueApp.Remove(id);

                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

    }
}
