﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using Viceri.Project.Manager.Application;
using Viceri.Project.Manager.Application.Interfaces;
using Viceri.Project.Manager.Application.ViewModels;

namespace Viceri.Project.Manager.Controllers
{
    public class ProjectsController : ApiController
    {
        private readonly IProjectApplication _projectApp;

        public ProjectsController(IProjectApplication projectApp)
        {
            _projectApp = projectApp;
        }

        public IHttpActionResult Get()
        {
            return Ok(_projectApp.GetAll());
        }

        public IHttpActionResult Get(int id)
        {
            try
            {
                var project = _projectApp.GetById(id);

                if (project == null)
                {
                    return BadRequest("Project Not Found");
                }

                return Ok(project);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        public IHttpActionResult Post([FromBody]ProjectViewModel project)
        {
            try
            {
                if (project == null)
                {
                    return BadRequest("Project Not Found");
                }

                _projectApp.AddOrUpdate(project);

                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        public IHttpActionResult Put([FromBody]ProjectViewModel project)
        {
            try
            {
                if (project == null)
                {
                    return BadRequest("Project Not Found");
                }

                _projectApp.AddOrUpdate(project);

                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        public IHttpActionResult Delete(int id)
        {
            try
            {
                _projectApp.Remove(id);

                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

    }
}
