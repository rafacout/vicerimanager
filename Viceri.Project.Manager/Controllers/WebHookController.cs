﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Viceri.Project.Manager.Application.Interfaces;
using Viceri.Project.Manager.Application.ViewModels.WebHook;

namespace Viceri.Project.Manager.Controllers
{
    public class WebHookController : ApiController
    {
        private readonly IWebHookApplication _hookApp;

        public WebHookController(IWebHookApplication hookApp)
        {
            _hookApp = hookApp;
        }

        public IHttpActionResult Post([FromBody]HookViewModel hook)
        {
            try
            {
                if (hook == null)
                {
                    return BadRequest("WebHook cannot be empty");
                }
              
                _hookApp.AddOrUpdate(hook);

                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        
    }
}
