﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Viceri.Project.Manager.Application;
using Viceri.Project.Manager.Application.Interfaces;

namespace Viceri.Project.Manager.Controllers
{
    public class ImportProjectsController : ApiController
    {
        private readonly IProjectApplication _projectApp;

        public ImportProjectsController(IProjectApplication projectApp)
        {
            _projectApp = projectApp;
        }


        public HttpResponseMessage Get()
        {
            var success = _projectApp.ImportGitProjects();

            if (success)
            {
                return Request.CreateResponse(HttpStatusCode.Created);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Importation fail");
            }
        }
        
    }
}
