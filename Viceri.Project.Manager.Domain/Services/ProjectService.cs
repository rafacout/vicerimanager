﻿using System;
using Viceri.Project.Manager.Domain.Interfaces.Repositories;
using Viceri.Project.Manager.Domain.Interfaces.Services;

namespace Viceri.Project.Manager.Domain.Services
{
    public class ProjectService : ServiceBase<Entities.Project>, IProjectService
    {
        private readonly IProjectRepository _projectRepository;
        private readonly IGitRepository _gitRepository;

        public ProjectService(IProjectRepository projectRepository, IGitRepository gitRepository)
            : base(projectRepository)
        {
            _projectRepository = projectRepository;
            _gitRepository = gitRepository;
        }

        public void AddOrUpdate(Entities.Project obj)
        {
            var issueGitId = _projectRepository.GetByProjectGitId(obj.ProjectGitId);

            if (obj.Id == 0)
            {
                if (issueGitId > 0)
                    throw new ApplicationException("Issue Id already exists!");

                _projectRepository.Add(obj);
            }
            else
            {
                if (obj.Id != issueGitId && issueGitId > 0)
                    throw new ApplicationException("Issue Id already exists!");

                _projectRepository.Update(obj);
            }
        }

        public bool ImportProjects()
        {
            var gitProjects = _gitRepository.GetAllProjects();

            foreach(var p in gitProjects)
            {
                var projectId = _projectRepository.GetByProjectGitId(p.ProjectGitId);

                if (projectId == 0)
                {
                    p.IsImported = true;
                    _projectRepository.Add(p);
                }
            }

            return true;
        }

        public int GetByProjectId(int id)
        {
            return _projectRepository.GetByProjectGitId(id);
        }
    }
}
