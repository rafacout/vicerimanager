﻿using System;
using System.Collections.Generic;
using Viceri.Project.Manager.Domain.Interfaces.Repositories;
using Viceri.Project.Manager.Domain.Interfaces.Services;

namespace Viceri.Project.Manager.Domain.Services
{
    public class ServiceBase<T> : IServiceBase<T> where T : class
    {
        private readonly IRepositoryBase<T> _repository;

        public ServiceBase(IRepositoryBase<T> repository)
        {
            _repository = repository;
        }

        public IEnumerable<T> GetAll()
        {
            return _repository.GetAll();
        }

        public T GetById(int id)
        {
            return _repository.GetById(id);
        }

        public void Remove(T obj)
        {
            _repository.Remove(obj);
        }
    }
}
