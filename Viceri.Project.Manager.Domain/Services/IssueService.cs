﻿using System;
using Viceri.Project.Manager.Domain.Interfaces.Repositories;
using Viceri.Project.Manager.Domain.Interfaces.Services;

namespace Viceri.Project.Manager.Domain.Services
{
    public class IssueService : ServiceBase<Entities.Issue>, IIssueService
    {
        private readonly IIssueRepository _issueRepository;

        public IssueService(IIssueRepository issueRepository)
            : base(issueRepository)
        {
            _issueRepository = issueRepository;
        }

        public void AddOrUpdate(Entities.Issue obj)
        {
            var issueGitId = _issueRepository.GetByIssueGitId(obj.IssueGitId);

            if (obj.Id == 0)
            {
                if (issueGitId > 0)
                    throw new ApplicationException("Issue Id already exists!");

                obj.Created_At = DateTime.Now;
                obj.Closed_At = null;

                _issueRepository.Add(obj);
            }
            else
            {
                if (obj.Id != issueGitId && issueGitId > 0)
                    throw new ApplicationException("Issue Id already exists!");

                _issueRepository.Update(obj);
            }
        }

        public int GetByIssueGitId(int id)
        {
            return _issueRepository.GetByIssueGitId(id);
        }
    }
}
