﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Viceri.Project.Manager.Domain.Entities
{
    public class Project
    {
        public int Id { get; set; }

        public int ProjectGitId { get; set; }

        public string Description { get; set; }

        public string Name { get; set; }

        public string Web_Url { get; set; }

        public int Open_Issues_Count { get; set; }

        public bool IsImported { get; set; }

    }
}
