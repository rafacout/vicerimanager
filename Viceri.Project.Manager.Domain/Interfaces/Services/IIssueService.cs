﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Viceri.Project.Manager.Domain.Interfaces.Services
{
    public interface IIssueService : IServiceBase<Entities.Issue>
    {
        void AddOrUpdate(Entities.Issue obj);

        int GetByIssueGitId(int id);
    }
}
