﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Viceri.Project.Manager.Domain.Interfaces.Services
{
    public interface IServiceBase<T> where T : class
    {
        T GetById(int id);

        IEnumerable<T> GetAll();

        void Remove(T obj);
    }
}
