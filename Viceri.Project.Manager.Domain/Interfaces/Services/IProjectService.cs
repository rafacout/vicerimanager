﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Viceri.Project.Manager.Domain.Interfaces.Services
{
    public interface IProjectService : IServiceBase<Entities.Project>
    {
        void AddOrUpdate(Entities.Project obj);

        bool ImportProjects();

        int GetByProjectId(int id);
    }
}
