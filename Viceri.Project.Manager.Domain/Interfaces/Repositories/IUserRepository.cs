﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Viceri.Project.Manager.Domain.Entities;

namespace Viceri.Project.Manager.Domain.Interfaces.Repositories
{
    public interface IUserRepository
    {
        User Login(string username, string password);
    }
}
