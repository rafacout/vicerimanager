﻿

namespace Viceri.Project.Manager.Domain.Interfaces.Repositories
{
    public interface IProjectRepository : IRepositoryBase<Entities.Project>
    {
        int GetByProjectGitId(int id);
    }
}
