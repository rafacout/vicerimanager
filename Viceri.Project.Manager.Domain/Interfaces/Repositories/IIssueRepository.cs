﻿

namespace Viceri.Project.Manager.Domain.Interfaces.Repositories
{
    public interface IIssueRepository : IRepositoryBase<Entities.Issue>
    {
        int GetByIssueGitId(int id);
    }
}
