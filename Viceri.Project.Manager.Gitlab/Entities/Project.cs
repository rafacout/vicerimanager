﻿
namespace Viceri.Project.Manager.Gitlab.Entities
{
    public class Project
    {
        public int Id { get; set; }

        public string Description { get; set; }

        public string Name { get; set; }

        public string Web_Url { get; set; }

        public int Open_Issues_Count { get; set; }

        public Project Forked_From_Project { get; set; }
    }
}
