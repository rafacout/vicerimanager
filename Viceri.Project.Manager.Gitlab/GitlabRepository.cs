﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using Viceri.Project.Manager.Domain.Interfaces.Repositories;

namespace Viceri.Project.Manager.Gitlab
{
    public class GitlabRepository : IGitRepository
    {
        private string _git_URL;
        private string _private_token;

        public GitlabRepository()
        {
            _git_URL = ConfigurationManager.AppSettings["git_url"];
            _private_token = ConfigurationManager.AppSettings["git_private_token"];
        }

        public List<Domain.Entities.Project> GetAllProjects()
        {
            List<Domain.Entities.Project> projects = new List<Domain.Entities.Project>();

            HttpClient client = new HttpClient();

            try
            {
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("private-token", _private_token);

                HttpResponseMessage responseGet = client.GetAsync(_git_URL + "projects").Result;

                if (responseGet.IsSuccessStatusCode)
                {
                    var gitProjects = responseGet.Content.ReadAsAsync<IEnumerable<Gitlab.Entities.Project>>().Result;

                    foreach (var p in gitProjects)
                    {
                        if (p.Forked_From_Project == null)
                        {
                            projects.Add(new Domain.Entities.Project() {
                                ProjectGitId = p.Id,
                                Description = p.Description,
                                Name = p.Name,
                                IsImported = true,
                                Web_Url = p.Web_Url
                            });
                        }
                    }
                }
                else
                {
                    var message = responseGet.Content.ReadAsStringAsync();
                    var error = JsonConvert.DeserializeObject<String>(message.Result);
                    throw new ApplicationException(error);
                }

                return projects;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                client.Dispose();
            }
        }
    }
}
