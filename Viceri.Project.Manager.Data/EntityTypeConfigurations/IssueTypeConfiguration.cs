﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Text;
using Viceri.Project.Manager.Domain.Entities;

namespace Viceri.Project.Manager.Data.EntityTypeConfigurations
{
    public class IssueTypeConfiguration : EntityTypeConfiguration<Domain.Entities.Issue>
    {
        public IssueTypeConfiguration()
        {
            this.ToTable("Issue");

            HasKey(c => c.Id);

            Property(c => c.IssueGitId)
                .IsRequired();

            Property(c => c.ProjectId)
                .IsRequired();

            Property(c => c.Title)
                .IsRequired()
                .HasMaxLength(1000);

            Property(c => c.Created_At)
                .IsRequired()
                .HasColumnType("datetime2");

            Property(c => c.Closed_At)
                .IsOptional()
                .HasColumnType("datetime2");

            Property(c => c.State)
                .IsRequired()
                .HasMaxLength(100);
        }
    }
}
