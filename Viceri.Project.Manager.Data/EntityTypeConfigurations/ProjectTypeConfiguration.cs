﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Text;
using Viceri.Project.Manager.Domain.Entities;

namespace Viceri.Project.Manager.Data.EntityTypeConfigurations
{
    public class ProjectTypeConfiguration : EntityTypeConfiguration<Domain.Entities.Project>
    {
        public ProjectTypeConfiguration()
        {
            this.ToTable("Project");

            HasKey(c => c.Id);

            Property(c => c.ProjectGitId)
                .IsRequired();

            Property(c => c.Description)
                .IsOptional()
                .HasMaxLength(1000);

            Property(c => c.Name)
                .IsRequired()
                .HasMaxLength(1000);

            Property(c => c.Web_Url)
               .IsRequired()
               .HasMaxLength(1000);
        }
    }
}
