﻿using System.Collections.Generic;
using System.Linq;
using Viceri.Project.Manager.Domain.Entities;
using Viceri.Project.Manager.Domain.Interfaces.Repositories;

namespace Viceri.Project.Manager.Data.Repositories
{
    public class IssueRepository : RepositoryBase<Domain.Entities.Issue>, IIssueRepository
    {
        private readonly ViceriProjectManagerContext _context;

        public IssueRepository(ViceriProjectManagerContext context) 
            : base(context)
        {
            _context = context;
        }

        public override Issue GetById(long id)
        {
            return _context.Issues.Include("Project").Where(a => a.Id == id).FirstOrDefault();
        }

        public override IEnumerable<Issue> GetAll()
        {
            return _context.Issues.Include("Project").ToList();
        }

        public int GetByIssueGitId(int id)
        {
            return _context.Issues.Where(a => a.IssueGitId == id).Select(b => b.Id).FirstOrDefault();
        }
    }
}
