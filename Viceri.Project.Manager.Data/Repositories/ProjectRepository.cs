﻿using System;
using System.Linq;
using Viceri.Project.Manager.Domain.Entities;
using Viceri.Project.Manager.Domain.Interfaces.Repositories;

namespace Viceri.Project.Manager.Data.Repositories
{
    public class ProjectRepository : RepositoryBase<Domain.Entities.Project>, IProjectRepository
    {
        private readonly ViceriProjectManagerContext _context;

        public ProjectRepository(ViceriProjectManagerContext context) 
            : base(context)
        {
            _context = context;
        }

        public int GetByProjectGitId(int id)
        {
            return _context.Projects.Where(a => a.ProjectGitId == id).Select(b => b.Id).FirstOrDefault();
        }

        public override void Remove(Domain.Entities.Project obj)
        {
            if (_context.Issues.Any(a => a.ProjectId == obj.Id))
                throw new ApplicationException("You cannot delete the project because it has issues associeted");

            _context.Projects.Remove(obj);
            _context.SaveChanges();
        }
    }
}
