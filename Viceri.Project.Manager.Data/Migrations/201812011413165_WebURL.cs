namespace Viceri.Project.Manager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class WebURL : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Project", "Web_Url", c => c.String(nullable: false, maxLength: 1000));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Project", "Web_Url");
        }
    }
}
