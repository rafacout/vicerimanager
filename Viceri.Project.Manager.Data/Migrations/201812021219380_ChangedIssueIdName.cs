namespace Viceri.Project.Manager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangedIssueIdName : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Project", "ProjectGitId", c => c.Int(nullable: false));
            AlterColumn("dbo.Issue", "Created_At", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Issue", "Closed_At", c => c.DateTime());
            DropColumn("dbo.Project", "ProjectId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Project", "ProjectId", c => c.Int(nullable: false));
            AlterColumn("dbo.Issue", "Closed_At", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Issue", "Created_At", c => c.DateTime());
            DropColumn("dbo.Project", "ProjectGitId");
        }
    }
}
