namespace Viceri.Project.Manager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DescriptionNotRequired : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Project", "Description", c => c.String(maxLength: 1000));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Project", "Description", c => c.String(nullable: false, maxLength: 1000));
        }
    }
}
