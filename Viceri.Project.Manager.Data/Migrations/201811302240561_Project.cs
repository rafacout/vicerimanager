namespace Viceri.Project.Manager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Project : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Project",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false, maxLength: 1000),
                        Name = c.String(nullable: false, maxLength: 1000),
                        IsImported = c.Boolean(nullable: false),
                        Forked_from_project_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Project", t => t.Forked_from_project_Id)
                .Index(t => t.Forked_from_project_Id);
            
            AlterColumn("dbo.User", "Username", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.User", "Password", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.User", "Email", c => c.String(nullable: false, maxLength: 1000));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Project", "Forked_from_project_Id", "dbo.Project");
            DropIndex("dbo.Project", new[] { "Forked_from_project_Id" });
            AlterColumn("dbo.User", "Email", c => c.String());
            AlterColumn("dbo.User", "Password", c => c.String());
            AlterColumn("dbo.User", "Username", c => c.String());
            DropTable("dbo.Project");
        }
    }
}
