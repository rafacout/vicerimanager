namespace Viceri.Project.Manager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OpenIssuesCount : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Project", "Open_Issues_Count", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Project", "Open_Issues_Count");
        }
    }
}
