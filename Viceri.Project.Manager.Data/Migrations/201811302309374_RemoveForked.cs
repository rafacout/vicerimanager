namespace Viceri.Project.Manager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveForked : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Project", "Forked_from_project_Id", "dbo.Project");
            DropIndex("dbo.Project", new[] { "Forked_from_project_Id" });
            DropColumn("dbo.Project", "Forked_from_project_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Project", "Forked_from_project_Id", c => c.Int());
            CreateIndex("dbo.Project", "Forked_from_project_Id");
            AddForeignKey("dbo.Project", "Forked_from_project_Id", "dbo.Project", "Id");
        }
    }
}
