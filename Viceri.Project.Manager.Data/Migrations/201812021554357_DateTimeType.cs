namespace Viceri.Project.Manager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DateTimeType : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Issue", "Created_At", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.Issue", "Closed_At", c => c.DateTime(precision: 7, storeType: "datetime2"));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Issue", "Closed_At", c => c.DateTime());
            AlterColumn("dbo.Issue", "Created_At", c => c.DateTime(nullable: false));
        }
    }
}
