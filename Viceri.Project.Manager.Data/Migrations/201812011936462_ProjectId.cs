namespace Viceri.Project.Manager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProjectId : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Project", "ProjectId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Project", "ProjectId");
        }
    }
}
