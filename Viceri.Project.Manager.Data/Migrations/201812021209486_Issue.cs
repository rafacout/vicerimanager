namespace Viceri.Project.Manager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Issue : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Issue",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IssueGitId = c.Int(nullable: false),
                        Title = c.String(nullable: false, maxLength: 1000),
                        Created_At = c.DateTime(),
                        Closed_At = c.DateTime(nullable: false),
                        State = c.String(nullable: false, maxLength: 100),
                        ProjectId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Project", t => t.ProjectId)
                .Index(t => t.ProjectId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Issue", "ProjectId", "dbo.Project");
            DropIndex("dbo.Issue", new[] { "ProjectId" });
            DropTable("dbo.Issue");
        }
    }
}
