﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Viceri.Project.Manager.Application.ViewModels.WebHook
{
    public class HookIssueViewModel
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string Created_At { get; set; }

        public string Closed_At { get; set; }

        public string State { get; set; }

    }
}
