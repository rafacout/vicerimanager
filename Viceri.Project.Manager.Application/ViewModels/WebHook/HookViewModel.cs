﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Viceri.Project.Manager.Application.ViewModels.WebHook
{
    public class HookViewModel
    {
        public string Object_Kind { get; set; }

        public string Event_Type { get; set; }

        public HookProjectViewModel Project { get; set; }

        public HookIssueViewModel Object_Attributes { get; set; }
    }
}
