﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Viceri.Project.Manager.Application.ViewModels
{
    public class IssueViewModel
    {
        public int Id { get; set; }

        public int IssueGitId { get; set; }

        public string Title { get; set; }

        public DateTime Created_At { get; set; }

        public DateTime Closed_At { get; set; }

        public string State { get; set; }

        public int ProjectId { get; set; }

        public ProjectViewModel Project { get; set; }
    }
}
