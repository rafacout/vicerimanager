﻿using Viceri.Project.Manager.Application.Extensions;
using AutoMapper;
using Viceri.Project.Manager.Application.Interfaces;
using Viceri.Project.Manager.Domain.Interfaces.Services;
using Viceri.Project.Manager.Application.ViewModels.WebHook;
using System;
using System.Globalization;

namespace Viceri.Project.Manager.Application
{
    public class WebHookApplication : IWebHookApplication
    {
        private readonly IIssueService _issueService;
        private readonly IProjectService _projectService;

        public WebHookApplication(IIssueService issueService, IProjectService projectService)
        {
            _issueService = issueService;
            _projectService = projectService;
        }

        public void AddOrUpdate(HookViewModel hook)
        {
            if (hook.Object_Kind == "issue")
            {
                Domain.Entities.Project _domainProject;
                Domain.Entities.Issue _domainIssue;

                int _projectId = _projectService.GetByProjectId(hook.Project.Id);

                if (_projectId == 0)
                {
                    _domainProject = new Domain.Entities.Project()
                    {
                        Id = 0,
                        ProjectGitId = hook.Project.Id,
                        Description = hook.Project.Description,
                        Name = hook.Project.Name,
                        Web_Url = hook.Project.Web_URL,
                        IsImported = true,
                        Open_Issues_Count = 0
                    };

                    _projectService.AddOrUpdate(_domainProject);
                }

                int _domainProjectId = _projectService.GetByProjectId(hook.Project.Id);


                int _issueId = _issueService.GetByIssueGitId(hook.Object_Attributes.Id);

                _domainIssue = new Domain.Entities.Issue()
                {
                    Id = _issueId,
                    IssueGitId = hook.Object_Attributes.Id,
                    ProjectId = _domainProjectId,
                    State = hook.Object_Attributes.State,
                    Title = hook.Object_Attributes.Title,
                    Created_At = DateTime.Parse(hook.Object_Attributes.Created_At.Replace(" UTC", "")).ToLocalTime()
                };

                if (!String.IsNullOrEmpty(hook.Object_Attributes.Closed_At))
                {
                    _domainIssue.Closed_At = DateTime.Parse(hook.Object_Attributes.Closed_At.Replace(" UTC", "")).ToLocalTime();
                }
                else
                {
                    _domainIssue.Closed_At = null;
                }


                _issueService.AddOrUpdate(_domainIssue);
            }
        }

    }
}