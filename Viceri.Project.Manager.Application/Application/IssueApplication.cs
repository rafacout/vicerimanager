﻿using System.Collections.Generic;
using Viceri.Project.Manager.Application.ViewModels;
using Viceri.Project.Manager.Application.Extensions;
using AutoMapper;
using Viceri.Project.Manager.Application.Interfaces;
using Viceri.Project.Manager.Domain.Interfaces.Services;

namespace Viceri.Project.Manager.Application
{
    public class IssueApplication : IIssueApplication
    {
        private readonly IIssueService _issueService;

        public IssueApplication(IIssueService issueService)
        {
            _issueService = issueService;
        }

        public void AddOrUpdate(IssueViewModel issue)
        {
            var obj = issue.MapTo<Domain.Entities.Issue>();

            _issueService.AddOrUpdate(obj);
        }

        public IEnumerable<IssueViewModel> GetAll()
        {
            var issues = Mapper.Map<IEnumerable<Domain.Entities.Issue>, IEnumerable<IssueViewModel>>(_issueService.GetAll());

            return issues;
        }

        public IssueViewModel GetById(int id)
        {
            var issue = _issueService.GetById(id);

            return issue.MapTo<IssueViewModel>();
        }

        public void Remove(int id)
        {
            var issue = _issueService.GetById(id);

            var obj = issue.MapTo<Domain.Entities.Issue>();

            _issueService.Remove(obj);
        }
    }
}
