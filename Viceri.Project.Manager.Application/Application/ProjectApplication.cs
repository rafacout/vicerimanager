﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Viceri.Project.Manager.Application.ViewModels;
using Viceri.Project.Manager.Domain.Interfaces;
using Viceri.Project.Manager.Application.Extensions;
using System.Security.Cryptography;
using AutoMapper;
using Viceri.Project.Manager.Application.Interfaces;
using Viceri.Project.Manager.Domain.Interfaces.Services;

namespace Viceri.Project.Manager.Application
{
    public class ProjectApplication : IProjectApplication
    {
        private readonly IProjectService _projectService;

        public ProjectApplication(IProjectService projectService)
        {
            _projectService = projectService;
        }

        public void AddOrUpdate(ProjectViewModel project)
        {
            var obj = project.MapTo<Domain.Entities.Project>();

            _projectService.AddOrUpdate(obj);
        }

        public IEnumerable<ProjectViewModel> GetAll()
        {
            var projects = Mapper.Map<IEnumerable<Domain.Entities.Project>, IEnumerable<ProjectViewModel>>(_projectService.GetAll());

            return projects;
        }

        public ProjectViewModel GetById(int id)
        {
            var project = _projectService.GetById(id);

            return project.MapTo<ProjectViewModel>();
        }

        public void Remove(int id)
        {
            var project = _projectService.GetById(id);

            var obj = project.MapTo<Domain.Entities.Project>();

            _projectService.Remove(obj);
        }

        public bool ImportGitProjects()
        {
            return _projectService.ImportProjects();
        }
    }
}
