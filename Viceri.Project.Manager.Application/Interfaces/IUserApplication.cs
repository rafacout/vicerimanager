﻿using Viceri.Project.Manager.Application.ViewModels;

namespace Viceri.Project.Manager.Application.Interfaces
{
    public interface IUserApplication
    {
        UserViewModel Login(string username, string password);
    }
}