﻿using System.Collections.Generic;
using Viceri.Project.Manager.Application.ViewModels;

namespace Viceri.Project.Manager.Application.Interfaces
{
    public interface IIssueApplication
    {
        void AddOrUpdate(IssueViewModel obj);

        IssueViewModel GetById(int id);

        IEnumerable<IssueViewModel> GetAll();

        void Remove(int obj);
    }
}