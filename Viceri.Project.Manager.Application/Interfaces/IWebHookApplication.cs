﻿using Viceri.Project.Manager.Application.ViewModels.WebHook;

namespace Viceri.Project.Manager.Application.Interfaces
{
    public interface IWebHookApplication
    {
        void AddOrUpdate(HookViewModel obj);
    }
}