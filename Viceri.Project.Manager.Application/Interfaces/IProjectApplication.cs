﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Viceri.Project.Manager.Application.ViewModels;

namespace Viceri.Project.Manager.Application.Interfaces
{
    public interface IProjectApplication
    {
        void AddOrUpdate(ProjectViewModel obj);

        ProjectViewModel GetById(int id);

        IEnumerable<ProjectViewModel> GetAll();

        void Remove(int obj);

        bool ImportGitProjects();
    }
}