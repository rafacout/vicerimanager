﻿using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Text;
using Viceri.Project.Manager.Application;
using Viceri.Project.Manager.Application.Interfaces;
using Viceri.Project.Manager.Data;
using Viceri.Project.Manager.Data.Repositories;
using Viceri.Project.Manager.Domain.Interfaces.Repositories;
using Viceri.Project.Manager.Domain.Interfaces.Services;
using Viceri.Project.Manager.Domain.Services;
using Viceri.Project.Manager.Gitlab;

namespace Viceri.Project.Manager.IoC
{
    public static class IoCConfiguration
    {
        public static void Configure(Container container)
        {
            ConfigureDomain(container);
            ConfigureData(container);
            ConfigureApplication(container);
        }

        private static void ConfigureApplication(Container container)
        {
            container.Register<IUserApplication, UserApplication>(Lifestyle.Scoped);
            container.Register<IProjectApplication, ProjectApplication>(Lifestyle.Scoped);
            container.Register<IIssueApplication, IssueApplication>(Lifestyle.Scoped);
            container.Register<IWebHookApplication, WebHookApplication>(Lifestyle.Scoped);

            container.Register<IUserRepository, UserRepository>(Lifestyle.Scoped);
            container.Register<IProjectRepository, ProjectRepository>(Lifestyle.Scoped);
            container.Register<IIssueRepository, IssueRepository>(Lifestyle.Scoped);
            container.Register<IGitRepository, GitlabRepository>(Lifestyle.Scoped);
        }

        private static void ConfigureData(Container container)
        {
            container.Register<ViceriProjectManagerContext>(Lifestyle.Scoped);
        }

        private static void ConfigureDomain(Container services)
        {
            services.Register<IUserService, UserService>(Lifestyle.Scoped);
            services.Register<IProjectService, ProjectService>(Lifestyle.Scoped);
            services.Register<IIssueService, IssueService>(Lifestyle.Scoped);
        }
    }
}
