﻿using Moq;
using Viceri.Project.Manager.Application;
using Viceri.Project.Manager.Domain.Interfaces.Services;
using Xunit;

namespace Viceri.Project.Manager.Tests.Application
{
    public class ProjectApplicationTest : UnitTestBase
    {
        private readonly Mock<IProjectService> _projectServiceMock;

        public ProjectApplicationTest()
        {
            _projectServiceMock = new Mock<IProjectService>();
        }


        [Fact]
        public void Get_Project_ById_Should_Be_Success()
        {
            _projectServiceMock.Setup(x => x.GetById(1)).Returns(new Domain.Entities.Project()
            {
                Id = 1,
                Description = "Project Test",
                IsImported = true,
                Name = "Project Name",
                Open_Issues_Count = 0,
                ProjectGitId = 123456,
                Web_Url = "http://gitlab.com"
            });

            var projectApplication = new ProjectApplication(_projectServiceMock.Object);

            var projectResult = projectApplication.GetById(1);

            Assert.Equal(1, projectResult.Id);
            Assert.Equal("Project Name", projectResult.Name);
            Assert.True(projectResult.IsImported);
            Assert.Equal(0, projectResult.Open_Issues_Count);
            Assert.Equal(123456, projectResult.ProjectGitId);
            Assert.Equal("http://gitlab.com", projectResult.Web_URL);
        }


        [Fact]
        public void Import_Projects_should_be_executed_with_success()
        {
            _projectServiceMock.Setup(x => x.ImportProjects()).Returns(true);

            var projectApplication = new ProjectApplication(_projectServiceMock.Object);

            var importResult = projectApplication.ImportGitProjects();

            Assert.True(importResult);
        }
    }
}
