﻿using Moq;
using System;
using System.Collections.Generic;
using Viceri.Project.Manager.Application;
using Viceri.Project.Manager.Application.ViewModels;
using Viceri.Project.Manager.Domain.Interfaces.Services;
using Xunit;

namespace Viceri.Project.Manager.Tests.Application
{
    public class IssueApplicationTest : UnitTestBase
    {
        private readonly Mock<IIssueService> _issueServiceMock;

        public IssueApplicationTest()
        {
            _issueServiceMock = new Mock<IIssueService>();
        }


        [Fact]
        public void Get_Issue_ById_Should_Be_Success()
        {
            var openedDate = new DateTime(2018, 10, 1);
            var closedDate = new DateTime(2018, 11, 5);

            _issueServiceMock.Setup(x => x.GetById(1)).Returns(new Domain.Entities.Issue()
            {
                Id = 1,
                ProjectId = 1,
                State = "closed",
                Title = "Issue Test",
                IssueGitId = 123456,
                Created_At = openedDate,
                Closed_At = closedDate
            });

            var issueApplication = new IssueApplication(_issueServiceMock.Object);

            var importResult = issueApplication.GetById(1);

            Assert.Equal(1, importResult.Id);
            Assert.Equal("Issue Test", importResult.Title);
            Assert.Equal(1, importResult.ProjectId);
            Assert.Equal(123456, importResult.IssueGitId);
            Assert.Equal("closed", importResult.State);
            Assert.Equal(openedDate, importResult.Created_At);
            Assert.Equal(closedDate, importResult.Closed_At);
        }


        [Fact]
        public void GetAll_Should_Return_2_Items()
        {
            _issueServiceMock.Setup(x => x.GetAll()).Returns(
                new List<Domain.Entities.Issue>()
                {
                    new Domain.Entities.Issue(){
                        Id = 1,
                        ProjectId = 1,
                        State = "closed",
                        Title = "Issue Test",
                        IssueGitId = 123456,
                        Created_At = DateTime.Now,
                        Closed_At = DateTime.Now
                    },
                    new Domain.Entities.Issue(){
                        Id = 2,
                        ProjectId = 1,
                        State = "opened",
                        Title = "Issue Test 2",
                        IssueGitId = 654321,
                        Created_At = DateTime.Now,
                        Closed_At = DateTime.Now
                    },
                });

            var issueApplication = new IssueApplication(_issueServiceMock.Object);

            var issueResult = issueApplication.GetAll();

            Assert.Equal(2, ((IList<IssueViewModel>)issueResult).Count);
        }


    }
}
